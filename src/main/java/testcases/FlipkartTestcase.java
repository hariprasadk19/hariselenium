package testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import cucumber.api.java.ht.E;

public class FlipkartTestcase {
	@Test
	public void flipkart() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.flipkart.com/");
		//driver.switchTo().alert().dismiss();
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		

//		allWindow.addAll(windowHandles);
//		driver.switchTo().window(allWindow.get(1));
//		driver.close();
//		driver.switchTo().window(allWindow.get(0));
		
		WebElement electronics = driver.findElementByXPath("//span[text()='Electronics']");
		
		Actions builder = new Actions(driver);
		builder.moveToElement(electronics).perform();
		
		
		driver.findElementByLinkText("Mi").click();
		Thread.sleep(3000);
		String chkTitle = driver.getTitle();
		System.out.println(chkTitle);
		if (chkTitle.contains("Mi")) {
			
			System.out.println("Page Title Matches");
		} else {
			System.out.println("Page Title Mismatched");
		}
		driver.findElementByXPath("//div[text()='Newest First']").click();
		Thread.sleep(3000);
		
		for(int i=1;i<25;i++) {
		String pdt1 = driver.findElementByXPath("(//div[@class='_1-2Iqu row'])["+i+"]/div[1]/div[1]").getText();
		String price = driver.findElementByXPath("(//div[@class='col col-5-12 _2o7WAb'])["+i+"]/div[1]/div/div").getText();
		System.out.println(pdt1+price);
		
		}
		String phoneName = driver.findElementByXPath("(//div[@class='_1-2Iqu row'])[1]/div[1]/div[1]").getText();
		driver.findElementByXPath("(//div[@class='_1-2Iqu row'])[1]/div[1]/div[1]").click();
		Thread.sleep(3000);
		Set<String> allWindowHandles = driver.getWindowHandles();
		List<String> allHandles = new ArrayList<>();
		allHandles.addAll(allWindowHandles);
		driver.switchTo().window(allHandles.get(1));
		String phoneTitle = driver.getTitle();
		if(phoneTitle.contains(phoneName)) {
			System.out.println("PhoneName page Title Matched");
		}else {
			System.out.println("Phone name page title mismatched");
		}
		
		String ratings = driver.findElementByXPath("(//span[@class='_38sUEc']/span/span)[1]").getText();
		String reviews = driver.findElementByXPath("(//span[@class='_38sUEc']/span/span)[3]").getText();
		
		System.out.println("Total number of ratings " +ratings );
		System.out.println("Total number of reviews " +reviews );
		driver.quit();
		
	}

}
